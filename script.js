function toggleMe() {
    let tabItems = document.querySelectorAll('.tabs li');
    let contentItems = document.querySelectorAll('.tabs-content li');

    function changeClass(event) {
        tabItems.forEach(item => { item.classList.remove('active') });
        event.target.classList.add('active');
        toggleContentItem(event.target);
    }

    function toggleContentItem(activeItem) {
        contentItems.forEach(item => {
            item.style.display = 'none';
            if (item.id === activeItem.dataset.name) item.style.display = 'block';
        });
    }

    function init() {
        tabItems.forEach(function (item, index) {
            item.dataset.name = item.textContent;
            contentItems[index].id = item.textContent;
            if (index === 0) toggleContentItem(item);
            item.addEventListener('click', changeClass);
        });
    }
    init();
}

document.addEventListener('DOMContentLoaded', function () {
    let MainFunc = toggleMe();
})








































